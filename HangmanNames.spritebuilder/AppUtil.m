//
//  AppUtil.m
//  HangmanNames
//
//  Created by Lyman Cao on 7/19/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "AppUtil.h"

@implementation AppUtil

+(CGFloat)resizeSprite:(CCSprite*)sprite toWidth:(float)width toHeight:(float)height {
    CGFloat ratioX =  1.0 * width / sprite.contentSize.width;
    CGFloat ratioY =  1.0 * height / sprite.contentSize.height;
    return ratioX > ratioY ?  ratioY : ratioX;
}
@end
