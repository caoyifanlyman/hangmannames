//
//  Key.h
//  HangmanNames
//
//  Created by Lyman Cao on 7/8/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CCNode.h"

@interface Key : CCNode

@property NSString * value;
@property CCNodeColor *backgroundNode;
- (void)updateValueDisplay;
- (void)guessKey:(BOOL)guessFlag;
- (void)restartKey;

@end
