//
//  MessageLayer.m
//  HangmanNames
//
//  Created by Lyman Cao on 7/17/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "MessageLayer.h"

@implementation MessageLayer{
    CCLayoutBox *_messageLayoutBox;
    CCNodeColor *_backgroundNode;
}

- (void)didLoadFromCCB {
    self.userInteractionEnabled = TRUE;
}

- (void)updateMessageLabel:(BOOL)won{
    if (won) {
        [_messageLabel setString:@"You won!"];
    }else{
        [_messageLabel setString:@"You lose!"];
    }
}

- (void)goHome{
    CCScene *gameplayScene = [CCBReader loadAsScene:@"MainScene"];
    [[CCDirector sharedDirector] replaceScene:gameplayScene];
}


@end
