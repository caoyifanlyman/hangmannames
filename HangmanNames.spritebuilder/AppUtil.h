//
//  AppUtil.h
//  HangmanNames
//
//  Created by Lyman Cao on 7/19/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppUtil : NSObject

+(CGFloat)resizeSprite:(CCSprite*)sprite toWidth:(float)width toHeight:(float)height;
@end
