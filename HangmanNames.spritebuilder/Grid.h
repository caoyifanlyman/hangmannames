//
//  Grid.h
//  HangmanNames
//
//  Created by Lyman Cao on 7/8/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CCNodeColor.h"

@interface Grid : CCNodeColor

@property NSArray* keyboard;
@end
