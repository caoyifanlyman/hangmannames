//
//  Grid.m
//  HangmanNames
//
//  Created by Lyman Cao on 7/8/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "Grid.h"
#import "Key.h"

@implementation Grid {
    CGFloat _columnWidth;
	CGFloat _columnHeight;
	CGFloat _keyMarginVertical;
	CGFloat _keyMarginHorizontal;
}

static const NSInteger ROW_SIZE = 7;
static const NSInteger COL_SIZE = 4;


- (void)setupBackground
{
	// load one tile to read the dimensions
	CCNode *prototypeKey = [CCBReader load:@"Key"];
	_columnWidth = prototypeKey.contentSizeInPoints.width;
	_columnHeight = prototypeKey.contentSizeInPoints.height;
    [prototypeKey performSelector:@selector(cleanup)];
    
	// calculate the margin by subtracting the tile sizes from the grid size
    
    _keyMarginHorizontal = (self.contentSizeInPoints.width - (ROW_SIZE * _columnWidth)) / (ROW_SIZE+1);
	_keyMarginVertical = (self.contentSizeInPoints.height - (COL_SIZE * _columnWidth)) / (COL_SIZE+1);
    
	// set up initial x and y positions
	float x = _keyMarginHorizontal;
	float y = self.contentSizeInPoints.height - _keyMarginVertical;

    char keyValue = 'A';
    NSMutableArray *growKeys = [NSMutableArray array];
    
	for (int i = 1; i < COL_SIZE; i++) {
		// iterate through each row
		x = _keyMarginHorizontal;
		for (int j = 0; j < ROW_SIZE; j++) {
			//  iterate through each column in the current row
            Key *currentKey = [self loadKeyWithX:x positionY:y value:keyValue++];

            [growKeys addObject:currentKey];
			[self addChild:currentKey];
			x+= _columnWidth + _keyMarginHorizontal;
		}
		y -= _columnHeight + _keyMarginVertical;
	}
    // the last five
    x = _keyMarginHorizontal + _columnWidth + _keyMarginHorizontal;
    for (int j = 0; j < ROW_SIZE - 2; j++) {
        //  iterate through each column in the current row
        Key *currentKey = [self loadKeyWithX:x positionY:y value:keyValue++];
        [growKeys addObject:currentKey];
        [self addChild:currentKey];
        x+= _columnWidth + _keyMarginHorizontal;
    }

    // load the local keyarray to keyboard
    _keyboard  = [NSArray arrayWithArray:growKeys];
}

-(void)cleanup{}

- (Key *)loadKeyWithX:(float)x positionY:(float)y value:(char)keyValue{
    Key *key = (Key *)[CCBReader load:@"Key"];
    
    // change keyValue
    key.value = [NSString stringWithFormat:@"%c", keyValue];
    [key updateValueDisplay];
    
    key.contentSize = CGSizeMake(_columnWidth, _columnHeight);
    key.position = ccp(x,y);
    return key;
}

- (void)didLoadFromCCB {
    self.userInteractionEnabled = TRUE;
	[self setupBackground];
}

- (CGPoint)positionForColumn:(NSInteger)column row:(NSInteger)row {
    // last line is different
    if (row == COL_SIZE) {
        column += 1;
    }
	NSInteger x = _keyMarginHorizontal + column * (_keyMarginHorizontal + _columnWidth);
	NSInteger y = _keyMarginVertical + row * (_keyMarginVertical + _columnHeight);
	return CGPointMake(x,y);
}




@end
