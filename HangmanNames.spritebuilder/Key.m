//
//  Key.m
//  HangmanNames
//
//  Created by Lyman Cao on 7/8/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "Key.h"
#import "Gameplay.h"

@implementation Key {
    CCLabelTTF *_valueLabel;
}
//
//- (id)init {
//    self = [super init];
//    if (self) {
//        // enable touch
//        self.userInteractionEnabled = TRUE;
//        self.value = @"A";
//    }
//    return self;
//}

- (void)didLoadFromCCB {
    self.userInteractionEnabled = TRUE;
    self.value = @"a";
    [self updateValueDisplay];
    _backgroundNode.color = [CCColor yellowColor];
}

- (void)guessKey:(BOOL)guessFlag{
    if (guessFlag) {
        [self updateColorDisplay:[CCColor greenColor]];
    }else{
        [self updateColorDisplay:[CCColor redColor]];
        [Gameplay changeKey:@""];
    }
    // disable touch feature
    self.userInteractionEnabled = FALSE;
}

- (void)restartKey{
    self.userInteractionEnabled = TRUE;
    _backgroundNode.color = [CCColor yellowColor];
}

- (void)updateValueDisplay {
    _valueLabel.string = [NSString stringWithString:self.value];
}

- (void)updateColorDisplay:(CCColor *)newColor {
    _backgroundNode.color = newColor;
}

- (void)touchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    
    [Gameplay changeKey:self.value];
    [super touchBegan:touch withEvent:event];
//    CCLOG(@"%@", self.value);
}

@end
