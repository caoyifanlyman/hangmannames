//
//  Gameplay.m
//  HangmanNames
//
//  Created by Lyman Cao on 7/1/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "Gameplay.h"
#import "Grid.h"
#import "Key.h"
#import "MessageLayer.h"
#import "AppUtil.h"

static NSString * keyPressed;
static NSString *gameCategory;


@implementation Gameplay{
    NSString *_stringDifficulty;
    NSString *_stringHiddenWord;
    int badGuessCount;
    
    CCLabelTTF *_lettersLabel;
    CCNode *_hangman;
    CCSprite *_hangmanSprite;
    CCNode *_wordPic;
    CCSprite *_wordPicSprite;
    CGFloat _imageWidth;
	CGFloat _imageHeight;
    Grid *_grid;
    MessageLayer *_messageLayer;
    
    NSDictionary *wordsCategories;
}

static NSInteger const GUESSTHRESHOLD = 8;

// is called when CCB file has completed loading
- (void)didLoadFromCCB {
    // init the _hangmanSprite and _wordPicSprite
    _hangmanSprite = [[CCSprite alloc] init];
    _wordPicSprite = [[CCSprite alloc] init];
    [_hangman addChild:_hangmanSprite];
    [_wordPic addChild:_wordPicSprite];
    
    _imageWidth = _hangman.contentSize.width;
    _imageHeight = _hangman.contentSize.height;
    
    // load from plist
    NSString *path = [[NSBundle mainBundle] pathForResource:@"WordsList" ofType:@"plist"];
    wordsCategories = [[NSDictionary alloc]initWithContentsOfFile:path];
    
    // current category
    NSArray *currentWordsList = wordsCategories[gameCategory];
    
    // random from plist
    int randomIdx = arc4random() % [currentWordsList count];
    NSDictionary * currentWord = currentWordsList[randomIdx];

    [self initGame:currentWord[@"name"] imageHint:currentWord[@"image"]];
}

-(void)initGame:(NSString*)inputWord imageHint:(NSString *)imageHint{
    self.userInteractionEnabled = TRUE;
    
    _stringHiddenWord = [inputWord uppercaseString];
    
    keyPressed = @"";
    badGuessCount = 1;
    [self stepHangman:1];
    
    // clean all grid keys
    for (int i = 0; i < _grid.keyboard.count; i++) {
        [_grid.keyboard[i] restartKey];
    }
    // init hidden word
    [self initWordLabel];
    
    // init hint image
    _wordPicSprite = [CCSprite spriteWithImageNamed: [NSString stringWithString:imageHint]];
    _wordPicSprite.position = ccp(_wordPic.contentSize.width * _wordPic.anchorPoint.x, _wordPic.contentSize.height * _wordPic.anchorPoint.y);
    _wordPicSprite.scale = [AppUtil resizeSprite:_wordPicSprite toWidth:_imageWidth toHeight:_imageHeight];

    [_wordPic addChild:_wordPicSprite];
}

- (void) initWordLabel{
    
    NSMutableString *labelString = [[NSMutableString alloc] init];
    for(int i=0; i<_stringHiddenWord.length;i++){
        if ([_stringHiddenWord characterAtIndex:i] == ' ') {
            [labelString appendString:@"  "];
        }else{
            [labelString appendString:@"_ "];
        }
        
    }
    [_lettersLabel setString:labelString];
}

- (void)stepHangman:(int) seq {
    
    // remove the old image first
    [_hangman removeChild:_hangmanSprite];
    
    NSString *hangmanImage = [NSString stringWithFormat:@"hangman%i.png", seq];
    _hangmanSprite = [CCSprite spriteWithImageNamed:hangmanImage];
    _hangmanSprite.scale = [AppUtil resizeSprite:_hangmanSprite toWidth:_imageWidth toHeight:_imageHeight];
    _hangmanSprite.position = ccp(_hangman.contentSize.width * _hangman.anchorPoint.x, _hangman.contentSize.height * _hangman.anchorPoint.y);
    
    [_hangman addChild:_hangmanSprite];
    
}

-(void) processGuess:(NSString *)guessedLetter{
    NSRange guessedLetterRange = [_stringHiddenWord rangeOfString:guessedLetter];
    
    int guessedIdx = [guessedLetter characterAtIndex:0] - 'A';
    
    // a bad key
    if(guessedLetterRange.location == NSNotFound){
        [_grid.keyboard[guessedIdx] guessKey:FALSE];
        
        badGuessCount++;
        
        // change the hangman image
        [self stepHangman:badGuessCount];
        
        if(badGuessCount >= GUESSTHRESHOLD){
            // show the result
            NSMutableString *labelString = [[NSMutableString alloc] init];
            for(int i=0; i<_stringHiddenWord.length;i++){
                [labelString appendFormat:@"%c ", [_stringHiddenWord characterAtIndex:i]];
            }
            [_lettersLabel setString:labelString];
            
            // lose the game
            [self gameEndTrigger:FALSE];
        }
    }else{ // a good guess!
        [_grid.keyboard[guessedIdx] guessKey:TRUE];
        
        NSRange foundInWord = guessedLetterRange;
        foundInWord.location = guessedLetterRange.location * 2;
        
        [_lettersLabel setString:[_lettersLabel.string stringByReplacingCharactersInRange:foundInWord withString:guessedLetter]];
        
        // replace ever character
        while(guessedLetterRange.location != NSNotFound){
            NSRange searchRange = guessedLetterRange;
            ++searchRange.location;
            searchRange.length = [_stringHiddenWord length] - searchRange.location;
            guessedLetterRange = [_stringHiddenWord rangeOfString:guessedLetter options:NSCaseInsensitiveSearch range:searchRange];
            
            if(guessedLetterRange.location!=NSNotFound){
                foundInWord.location=guessedLetterRange.location*2;
                _lettersLabel.string = [_lettersLabel.string stringByReplacingCharactersInRange:foundInWord withString:guessedLetter];
            }
        }
        
        if ([_lettersLabel.string rangeOfString:@"_"].location == NSNotFound) {
            // win the game
            [self gameEndTrigger:TRUE];
        }
    }
}

- (void)gameEndTrigger:(BOOL)won {
    // disable on touch anymore
    self.userInteractionEnabled = FALSE;
    
    _messageLayer = (MessageLayer *)[CCBReader load:@"MessageLayer"];
    [_messageLayer updateMessageLabel:won];
    [_messageLayer.nextButton setTarget:self selector:@selector(nextWord:)];
    
    [self addChild: _messageLayer];
}

-(void)nextWord:(id)sender{
    [[CCDirector sharedDirector] replaceScene:[CCBReader loadAsScene:@"Gameplay"]];
}

- (void)touchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    if (![keyPressed isEqualToString:@""]) {
        [self processGuess:keyPressed];
    }
}

-(void)goBackHome{
    [[CCDirector sharedDirector] replaceScene:[CCBReader loadAsScene:@"MainScene"]];
}

+(void)chooseGameCategory:(NSString *)toChooseCategory{
    gameCategory = [NSString stringWithString:toChooseCategory];
}


+(void)changeKey:(NSString *)newKey{
    keyPressed = [NSString stringWithString:newKey];
}

@end
