//
//  Gameplay.h
//  HangmanNames
//
//  Created by Lyman Cao on 7/1/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CCNode.h"

@interface Gameplay : CCNode

+(void)chooseGameCategory:(NSString *)toChooseCategory;

+(void)changeKey:(NSString *)keyPressed;

@end
