//
//  MainScene.m
//  PROJECTNAME
//
//  Created by Viktor on 10/10/13.
//  Copyright (c) 2013 Apportable. All rights reserved.
//

#import "MainScene.h"
#import "Gameplay.h"


@implementation MainScene{
    NSString *_category;
    CCLayoutBox *_categoryLayer;
}

static NSInteger const BUTTONNUMBERS = 5;

- (void)didLoadFromCCB {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"WordsList" ofType:@"plist"];
    NSArray *allCategories = [[[NSDictionary alloc]initWithContentsOfFile:path] allKeys];
    
    _categoryLayer.anchorPoint = ccp(0.5, 0.5);
    
    // load into categoryLayer
    for (int i = 0; i < [allCategories count]; i++) {
        CCButton *curButton = [CCButton buttonWithTitle:allCategories[i] fontName:@"Helvetica" fontSize:17.0f];
        NSString *buttonName = [NSString stringWithFormat:@"buttons%d.png", i % BUTTONNUMBERS];
        CCSprite *buttonSprite = [CCSprite spriteWithImageNamed:buttonName];
        buttonSprite.scale = 0.5;
        
        [curButton setBackgroundSpriteFrame:buttonSprite.spriteFrame forState:CCControlStateNormal];
        [curButton setPreferredSize:CGSizeMake(243, 63)];
        
        [curButton setTarget:self selector:@selector(chooseCategory:)];
        [_categoryLayer addChild:curButton];
    }
}

- (void)chooseCategory:(id)sender{
    [Gameplay chooseGameCategory:((CCButton *)sender).title];

    CCScene *gameplayScene = [CCBReader loadAsScene:@"Gameplay"];
    [[CCDirector sharedDirector] replaceScene:gameplayScene];
}

@end
