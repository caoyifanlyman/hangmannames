//
//  MessageLayer.h
//  HangmanNames
//
//  Created by Lyman Cao on 7/17/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CCScene.h"

@interface MessageLayer : CCScene

@property CCLabelBMFont *messageLabel;
@property CCButton *nextButton;

- (void)updateMessageLabel:(BOOL) won;
@end
